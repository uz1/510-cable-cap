// This is a 90 degree end cap for a 510 extension cable

// Imports
// Open source library https://github.com/rcolyer/threads-scad/blob/master/threads.scad
include <threads-scad/threads.scad>;

// silence the demo generation
module Demo(){}

// Variables
$test = false;

// 510 female diameters
$510_female_diameter = 22;
$510_female_thickness = 1;

// Thread settings
$thread_tolerance = 0.4;
$thread_cap_height = 3.5;

// Padding
$padding = 2;

// Dimensions
$cap_height = 22;

// Cable
$cable_diameter = 7;

// Globals
$fn = 300;

// The main show
translate([0, 0, 0]){
    tube();
}
translate([-$510_female_diameter - 8, 0, 0]){
    top_cap();
}
translate([$510_female_diameter + 8, 0, 0]){
    bottom_cap();
}

// Modules

// The cap
module tube(){
    difference(){
        // Positives
        union(){
            cylinder(d = $510_female_diameter + $padding + 3, h = $cap_height);
        }

        // Negatives
        union(){
            translate([0, 0, 2])cylinder(d = $510_female_diameter - $padding, h = $cap_height);

            // Negative for top screw top
            translate([0, 0, 18.6]){
                ScrewThread(outer_diam = $510_female_diameter + $padding, height = $thread_cap_height + 0.2, pitch = 0.8);
            }

            // Negative for bottom screw cap
            translate([0, 0, -0.1])ScrewThread(outer_diam = $510_female_diameter + $padding, height = $thread_cap_height + 0.2, pitch = 0.8);

            // Opening for cable
            translate([0, 0, -0.01]){
                translate([0, 0, $thread_cap_height + 0.5 * $cable_diameter]){
                    rotate([-90, 0, 0]){
                        cylinder(d = $cable_diameter, h = $510_female_diameter);
                    }
                }
                translate([-0.5 * $cable_diameter, 0, 0]){
                    cube([$cable_diameter, 15, $thread_cap_height + 0.5 * $cable_diameter]);
                }
            }
        }
    }
}

module top_cap(){
    difference(){
        ScrewThread(outer_diam = $510_female_diameter + $padding - $thread_tolerance, height = $thread_cap_height, pitch = 0.8);


        // Negatives
        union(){
            translate([0, 0, -0.01]){
                cylinder(d = 10.5, h = $thread_cap_height + $510_female_thickness + 0.1);
            }
        }



    }
}

module bottom_cap(){
    difference(){
        ScrewThread(outer_diam = $510_female_diameter + $padding - $thread_tolerance, height = $thread_cap_height, pitch = 0.8);

        translate([0, 0, 0.99]){
            cube([18, 2, 2], center = true);
        }
    }
}
