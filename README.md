# 510 Cable Threaded Cap

This design is a threaded end cap for 510 female connectors such as https://www.varitube.com/510-Connectors_b_2.html

All parameters are set as global variables so the design can adjust to whatever size 510 female conector needed. Each part, the shaft, top cap, and bottom cap are each their own module so you can easily include or exclude each part.

This design relies on the threads.scad submodule from https://github.com/rcolyer/threads-scad

## Usage
All parts can be output by translating each away from another
```
translate([0, 0, 0]){
    tube();
}
translate([-$510_female_diameter - 8, 0, 0]){
    top_cap();
}
translate([$510_female_diameter + 8, 0, 0]){
    bottom_cap();
}
```
